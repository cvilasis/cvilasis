#include <SoftwareSerial.h>
#define DEBUG true
SoftwareSerial esp8266(2,3);

void setup()
{
  Serial.begin(19200);
  esp8266.begin(19200);
  pinMode(10, OUTPUT);
  digitalWrite(10, LOW);
  pinMode(11, OUTPUT);
  digitalWrite(11, LOW);
  pinMode(12, OUTPUT);
  digitalWrite(12, LOW);
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  //Reiniciar módulo
  sendData("AT+RST\r\n", 2000, DEBUG);
  //Configurar acceso
  //1.station; 2.access poinr; 3.access point + station
  sendData("AT+CWMODE=2\r\n", 1000, DEBUG);
  //Recupera ip - 192.168.4.1
  sendData("AT+CIFSR\r\n", 1000, DEBUG);
  //Configura para aceptar múltiples conexiones
  //0.single; 1.multiple
  sendData("AT+CIPMUX=1\r\n", 1000, DEBUG);
  //Configura servidor en puerto 80
  //0.remove server; 1.add server
  sendData("AT+CIPSERVER=1,80\r\n", 1000, DEBUG);
  //Recupera ip - 192.168.4.1
  sendData("AT+CIFSR\r\n", 1000, DEBUG);
}

void loop()
{
  //Para simular pin elimina estas líneas
  /*int pinDebug = 10;
  if((pinDebug>9) and (pinDebug<14))
  {
	digitalWrite(pinDebug, !digitalRead(pinDebug));
    delay(500);
  }*/

  //loop
  if(esp8266.available())
  {
    if(esp8266.find("+IPD,"))
    {
      delay(1000);
      int connectionId = esp8266.read()-48;
      esp8266.find("pin=");
      //Recoger ASCII decenas + unidades -> conertir a número
      int pinNumber = (esp8266.read()-48)*10;
      pinNumber += esp8266.read()-48;
      //Cambia estado del pin escogido
      if((pinNumber>9) and (pinNumber<14))
      {
        digitalWrite(pinNumber, !digitalRead(pinNumber));
        String closeCommand = "AT+CIPCLOSE=";
        closeCommand += connectionId;
        closeCommand += "\r\n";
        sendData(closeCommand, 1000, DEBUG);
      }
    }
  }
}

String sendData(String command, int timeout, boolean debug)
{
  String response = "";
  esp8266.print(command);
  long int time = millis();
  while((time+timeout) > millis())
  {
    while(esp8266.available())
    {
      response += esp8266.read();
    }
  }
  if(debug)
  {
    Serial.print(response);
  }
  return response;
}